import java.util.Arrays;
public class sortName {
public static void insertionSort(String[] firstNames, String[] lastNames) {
        for (int i = 1; i < firstNames.length; i++) {
        String currentFirstName = firstNames[i];
        String currentLastName = lastNames[i];
        int j = i - 1;
        while (j >= 0 && (firstNames[j].compareTo(currentFirstName) > 0 || (firstNames[j].compareTo(currentFirstName) == 0 && lastNames[j].compareTo(currentLastName) > 0))) {
        firstNames[j + 1] = firstNames[j];
        lastNames[j + 1] = lastNames[j];
        j--;
        }
        firstNames[j + 1] = currentFirstName;
        lastNames[j + 1] = currentLastName;
        }
        }

public static void main(String[] args) {
        String[] firstNames = {"Egor", "Artiom", "Egor", "Vlad", "Igor","Ivan","Kirill"};
        String[] lastNames = {"Anufriev", "Abikenov", "Kalinchuk", "Ilyin", "Logvinov","Minin","Fomichev"};
        insertionSort(firstNames, lastNames);
        System.out.println("Отсортированный массив имен: " + Arrays.toString(firstNames));
        System.out.println("Отсортированный массив фамилий: " + Arrays.toString(lastNames));
        }
}

