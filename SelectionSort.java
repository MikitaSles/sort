import java.util.Arrays;

public class SelectionSort {
    public static void main(String[] args){
        char[] array= {'c','a','e','d','b'};
        selectionSort(array);
        System.out.println("Selection sort: " + Arrays.toString(array));

        bubbleSort(array);
        System.out.println("Bubble sort: " + Arrays.toString(array));

        insertionSort(array);
        System.out.println("Insertion sort: " + Arrays.toString(array));
    }

    public static void selectionSort(char[] array) {
        for (int i = 0; i < array.length; i++) {
            int minIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }
            char temp = array[i];
            array[i] = array[minIndex];
            array[minIndex] = temp;
        }
    }
    public static void bubbleSort(char[] array) {
        boolean sorted = false;
        while (!sorted) {
            sorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    char temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                    sorted = false;
                }
            }
        }
    }
    public static void insertionSort(char[] array) {
        for (int i = 1; i < array.length; i++) {
            char current = array[i];
            int j = i - 1;
            while (j >= 0 && array[j] > current) {
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = current;
        }
    }
}